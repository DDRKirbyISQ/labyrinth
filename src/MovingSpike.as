package  
{
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MovingSpike extends Transitionable 
	{
		[Embed(source='../img/movingspike.png')]
		private static const kSpritemapFile:Class;
		private var _spritemap:Spritemap = new Spritemap(kSpritemapFile, 10, 10);
		
		private var _x:Number = 0;
		
		private var _goingRight:Boolean;
		
		private static const kDistance:int = 5;
		
		public function MovingSpike(x:Number, y:Number, goingRight:Boolean = true) {
			super(x, y, _spritemap);
			type = "movingSpike";
			layer = -50;
			setHitbox(kWidth, kHeight);
			_goingRight = goingRight;
			if (!goingRight) {
				_x = kDistance * kWidth;
			}
			
			_spritemap.add("spike", [0, 1, 2, 3, 4], 0.33);
			_spritemap.play("spike");
		}
		
		override public function update():void 
		{
			super.update();
			
			if (IsTransitioning()) {
				return;
			}
			
			_spritemap.flipped = !_goingRight;
			
			// Update positioning.
			if (_x <= 0) {
				_goingRight = true;
			}
			if (_x >= kDistance * kWidth) {
				_goingRight = false;
			}
			if (_goingRight) {
				_x += 1;
			} else {
				_x -= 1;
			}
			
			_spritemap.originX = -_x;
			originX = -_x;
			
			
			// Check for collisions.
			if (collideWith(Player.Instance, x, y)) {
				Player.Instance.Die();
			}
		}
	}
}
