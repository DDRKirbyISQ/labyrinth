package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class BlueBlock extends Transitionable {
		[Embed(source='../img/blueblock.png')]
		private static const kImageFile:Class;
		private var _image:Image = new Image(kImageFile);
		
		[Embed(source='../img/blueblockdisabled.png')]
		private static const kDisabledImageFile:Class;
		private var _disabledImage:Image = new Image(kDisabledImageFile);
		
		public function BlueBlock(x:Number, y:Number) {
			super(x, y, _image);
			type = "block";
			layer = -50;
			setHitbox(kWidth, kHeight);
			_disabledImage.alpha = 1;
			_image.alpha = 0;
			addGraphic(_disabledImage);
		}
		
		override public function update():void 
		{
			super.update();
			
			collidable = _image.alpha >= 0.5;
			if (!Switch.RedEnabled) {
				_disabledImage.alpha -= 0.1;
				_image.alpha += 0.1;
			} else {
				_disabledImage.alpha += 0.1;
				_image.alpha -= 0.1;
			}
		}
	}
}
