package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class GreenBlock extends Transitionable {
		[Embed(source='../img/greenblock.png')]
		private static const kImageFile:Class;
		private var _image:Image = new Image(kImageFile);
		
		[Embed(source='../img/greenblockdisabled.png')]
		private static const kDisabledImageFile:Class;
		private var _disabledImage:Image = new Image(kDisabledImageFile);
		
		public function GreenBlock(x:Number, y:Number) {
			super(x, y, _image);
			type = "block";
			layer = -50;
			setHitbox(kWidth, kHeight);
			_disabledImage.alpha = 1;
			_image.alpha = 0;
			addGraphic(_disabledImage);
		}
		
		override public function update():void 
		{
			super.update();
			
			collidable = _image.alpha >= 0.5;
			if (!SwitchGreen.YellowEnabled) {
				_disabledImage.alpha -= 0.1;
				_image.alpha += 0.1;
			} else {
				_disabledImage.alpha += 0.1;
				_image.alpha -= 0.1;
			}
		}
	}
}
