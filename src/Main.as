package {
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	[Frame(factoryClass="Preloader")]
	
	public class Main extends Engine {
		public static const DEBUG:Boolean = false;
		
		public function Main() {
			super(500, 300, 60, true);
			FP.screen.scale = 2;
			FP.screen.color = 0xFFFFFF;
			
			// Debug console.
			if (DEBUG) {
				FP.console.enable();
				FP.console.visible = false;
				
				Levels.LevelChecks();
			}
			
			FP.randomizeSeed();
			
			// Start game.
			Input.define("jump", Key.SPACE, Key.UP);
			FP.world = new GameWorld();
		}
	}
}
