package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Transitionable extends Entity {
		public static const kWidth:int = 10;
		public static const kHeight:int = 10;
		
		public static const kDuration:int = 80;
		
		private var _targetX:Number;
		private var _targetY:Number;
		private var _startX:Number;
		private var _startY:Number;
		private var _progress:Number;
		
		private var _fading:Boolean = false;
		
		public function Transitionable(x:Number = 0, y:Number = 0, graphic:Graphic = null, mask:Mask = null) {
			super(x, y, graphic, mask);
			_targetX = x;
			_targetY = y;
			_progress = 1.0;
			_startX = x;
			_startY = y;
		}
		
		override public function update():void {
			super.update();
			
			_progress = Math.min(_progress + 1.0 / kDuration, 1.0);
			x = _progress * (_targetX - _startX) + _startX;
			y = _progress * (_targetY - _startY) + _startY;
			
			if (_progress >= 1.0 && _fading) {
				GameWorld.Instance.remove(this);
			}
		}
		
		public function Transition(x:Number, y:Number):void {
			_startX = this.x;
			_startY = this.y;
			_targetX = x;
			_targetY = y;
			_progress = 0;
		}
		
		public function FadeAway():void {
			_fading = true;
			var position:Position = Levels.RandOutside();
			Transition(position.X, position.Y);
		}
		
		public function IsTransitioning():Boolean {
			return _progress < 1.0;
		}
	}
}