package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class SwitchGreen extends Transitionable {
		public static var YellowEnabled:Boolean = true;
		
		[Embed(source='../img/switchyellow.png')]
		private static const kYellowImageFile:Class;
		private var _yellowImage:Image = new Image(kYellowImageFile);
		
		[Embed(source='../img/switchgreen.png')]
		private static const kGreenImageFile:Class;
		private var _greenImage:Image = new Image(kGreenImageFile);
		
		[Embed(source='../img/switchyellowdown.png')]
		private static const kYellowDownImageFile:Class;
		private var _yellowDownImage:Image = new Image(kYellowDownImageFile);
		
		[Embed(source='../img/switchgreendown.png')]
		private static const kGreenDownImageFile:Class;
		private var _greenDownImage:Image = new Image(kGreenDownImageFile);
		
		[Embed(source='../sfx/switch.mp3')]
		private static const kSfxFile:Class;
		
		private var _pressing:Boolean = false;
		
		public function SwitchGreen(x:Number, y:Number) {
			super(x, y, _yellowImage);
			type = "switchGreen";
			layer = 10;
			setHitbox(kWidth, kHeight);
			
			_greenImage.visible = false;
			addGraphic(_greenImage);
			addGraphic(_greenDownImage);
			addGraphic(_yellowDownImage);
		}
		
		override public function update():void {
			super.update();
			
			_yellowImage.visible = YellowEnabled && !_pressing;
			_yellowDownImage.visible = YellowEnabled && _pressing;
			_greenImage.visible = !YellowEnabled && !_pressing;
			_greenDownImage.visible = !YellowEnabled && _pressing;

			if (IsTransitioning()) {
				return;
			}
			
			if (collideWith(Player.Instance, x, y) || collide("movingSpike", x, y) || collide("movingSpikeLeft", x, y)) {
				if (!_pressing) {
					YellowEnabled = !YellowEnabled;
					_pressing = true;
					
					new Sfx(kSfxFile).play();
				}
			} else {
				_pressing = false;
			}
		}
		
		override public function Transition(x:Number, y:Number):void 
		{
			super.Transition(x, y);
			YellowEnabled = true;
		}
	}

}
