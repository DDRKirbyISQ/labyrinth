package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Emitter;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class LevelKey extends Entity {
		[Embed(source='../img/key.png')]
		private static const kImageFile:Class;
		private var _image:Image = new Image(kImageFile);
		
		[Embed(source='../img/particlemedium.png')]
		private const particleFile:Class;
		
		private var _emitter:Emitter;
		private var _timer:int = 0;
		
		public function LevelKey(x:Number, y:Number) {
			super(x - 3, y - 6, _image);
			layer = -100;
			setHitbox(_image.width, _image.height);
			
			_emitter = new Emitter(particleFile, 3, 3);
			_emitter.newType("regular", [0]);
			_emitter.setAlpha("regular", 1, 0);
			_emitter.setMotion("regular", 0, 10, 30, 360, 25, 10);
			_emitter.setColor("regular", 0xFFFF80, 0xFFFF80);
			addGraphic(_emitter);
		}
		
		override public function update():void 
		{
			super.update();
			
			if (collideWith(Player.Instance, x, y)) {
				GameWorld.Instance.remove(this);
				GameWorld.NextLevel();
				Player.Instance.ResetSpawn();
				return;
			}
			
			_timer++;
			if (_timer % 3 == 0) {
			_emitter.emit("regular", 8, 8);
			}
		}
	}
}
