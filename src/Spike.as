package  
{
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Spike extends Transitionable 
	{
		[Embed(source='../img/spike.png')]
		private static const kImageFile:Class;
		private var _image:Image = new Image(kImageFile);
		
		public function Spike(x:Number, y:Number) {
			super(x, y, _image);
			type = "spike";
			layer = -50;
			setHitbox(kWidth, kHeight);
		}
		
		override public function update():void 
		{
			super.update();
			
			if (IsTransitioning()) {
				return;
			}
			
			// Check for collisions.
			if (collideWith(Player.Instance, x, y)) {
				Player.Instance.Die();
			}
		}
	}
}
