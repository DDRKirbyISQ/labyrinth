package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Emitter;
	import flash.display.BitmapData;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Torch extends Entity {
		[Embed(source='../img/particlemedium.png')]
		private const particleFile:Class;
		
		private var _emitter:Emitter;
		private var _light:Light;
		
		[Embed(source='../img/torchlight.png')]
		private static const kLightImageFile:Class;
		private var _lightImage:Image = new Image(kLightImageFile);
		
		[Embed(source='../img/torch.png')]
		private static const kImageFile:Class;
		private var _spritemap:Spritemap = new Spritemap(kImageFile, 30, 30);
		
		public function Torch() {
			type = "torch";
			_lightImage.originX = _lightImage.width / 2;
			_lightImage.originY = _lightImage.height / 2;
			
			setHitbox(30, 30);
			graphic = _spritemap;
			_spritemap.originX = width / 2;
			_spritemap.originY = height / 2;
			_spritemap.add("torch", [0, 1, 2], 0.1 + FP.random * 0.05);
			graphic = _spritemap;
			_spritemap.play("torch");
			
			_emitter = new Emitter(particleFile, 3, 3);
			_emitter.newType("regular", [0]);
			_emitter.setAlpha("regular", 1, 0);
			_emitter.setMotion("regular", 0, 25, 30, 360, 25, 30);
			_emitter.setColor("regular", 0xFFFF80, 0xFF8080);
			addGraphic(_emitter);
			
			layer = 50;
		}
		
		public function Init(x:Number, y:Number):void {
			this.x = x;
			this.y = y;
			
			_light = new Light(x, y, _lightImage, 1.0, 1.0, 0.0);
			GameWorld.Instance.LightingObject.add(_light);
		}
		
		override public function update():void {
			super.update();
			
			if (distanceFrom(Player.Instance) < 50) {
				_light.alpha += 0.01;
			}
			
			if (FP.random < 0.1) {
				_emitter.emit("regular", 0, -10);
			}
		}
	}
}
