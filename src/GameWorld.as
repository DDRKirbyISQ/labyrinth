package {
	import flash.automation.KeyboardAutomationAction;
	import flash.display.DisplayObjectContainer;
	import flash.globalization.DateTimeFormatter;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Screen;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	import punk.fx.graphics.FXImage;
	import punk.fx.effects.PixelateFX;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class GameWorld extends World {
		private static const kMusicVolume:Number = 0.9;
		
		[Embed(source='../img/background.png')]
		private static const kBackgroundImageFile:Class;
		private var _backgroundImage:Image = new Image(kBackgroundImageFile);
		
		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var _blackImage:Image = new Image(kBlackImageFile);

		[Embed(source='../sfx/falling.mp3')]
		private static const kFallingSfxFile:Class;
		
		[Embed(source='../sfx/fall.mp3')]
		private static const kFallSfxFile:Class;
		
		[Embed(source='../mus/intro.mp3')]
		private static const kIntroMusicFile:Class;
		private static var _introMusic:Sfx = new Sfx(kIntroMusicFile);
		
		[Embed(source='../mus/area_1.mp3')]
		private static const kArea1MusicFile:Class;
		private static var _area1Music:Sfx = new Sfx(kArea1MusicFile);

		[Embed(source='../mus/area_2.mp3')]
		private static const kArea2MusicFile:Class;
		private static var _area2Music:Sfx = new Sfx(kArea2MusicFile);

		[Embed(source='../mus/area_3.mp3')]
		private static const kArea3MusicFile:Class;
		private static var _area3Music:Sfx = new Sfx(kArea3MusicFile);

		public var LightingObject:Lighting;
		
		public static var Instance:GameWorld;
		
		public var Timer:int = 0;
		
		public var Level:int = 0;
		
		public function GameWorld() {
			FP.screen.color = 0x000000;
			Instance = this;
			
			addGraphic(_backgroundImage, 100);
			
			_blackImage.scale = 600;
			addGraphic(_blackImage, -4000);

			LightingObject = new Lighting(FP.width, FP.height, 0x000000, -1000);
			//add(LightingObject);
			
			add(new Player(40, 239));

			if (Main.DEBUG && false) {
				Levels.LoadLevel(11);
				Timer = 600;
				Level = 11;
				_blackImage.alpha = 0;
				_area2Music.loop();
			} else {
				Levels.LoadLevel(0);
				Player.Instance.active = false;
				Player.Instance.visible = false;
			}
		}
		
		override public function update():void {
			super.update();
		
			// Debug stuff.
			if (Main.DEBUG) {
				if (Input.pressed(Key.Z)) {
					FP.console.visible = !FP.console.visible;
				}
				if (Input.pressed(Key.ENTER)) {
					NextLevel();
				}
				if (Input.check(Key.W)) {
					Player.Instance.y -= 5;
				}
				if (Input.check(Key.R)) {
					Player.Instance.y += 5;
				}
				if (Input.check(Key.A)) {
					Player.Instance.x -= 5;
				}
				if (Input.check(Key.S)) {
					Player.Instance.x += 5;
				}
			}
			
			Timer++;
			
			// Handle special events.

			// Intro.
			if (Timer < 90) {
				_blackImage.alpha -= 1.0 / 60;
			}
			if (Timer == 60) {
				_introMusic.play(kMusicVolume);
			}
			if (Timer == 62) {
				NextLevel();
			}
			if (Timer == 302) {
				NextLevel();
			}
			if (Timer == 459) {
				NextLevel();
			}
			if (Timer == 539) {
				var flash:ScreenFlash = create(ScreenFlash) as ScreenFlash;
				flash.reset(0xFFFFFF, 1.0);
				Player.Instance.active = true;
				Player.Instance.visible = true;
				Player.Instance._freezeTimer = 0;
				_area1Music.loop(kMusicVolume);
			}
			
			if (Level == 7) {
				if (_area1Music.playing) {
					_area1Music.volume -= 0.1;
					if (_area1Music.volume <= 0) {
						_area1Music.stop();
						_area2Music.loop(kMusicVolume);
					}
				}
			}
			
			if (Level == 13) {
				if (_area2Music.playing) {
					_area2Music.volume -= 0.01;
					if (_area2Music.volume <= 0) {
						_area2Music.stop();
					}
				}
			}
			if (Level == 14) {
				if (_area2Music.playing) {
					_area2Music.stop();
				}
				if (!_area3Music.playing) {
					_area3Music.loop(kMusicVolume);
				}
			}
		}
		
		public static function NextLevel():void {
			Instance.Level++;
			Levels.LevelTransition(Instance.Level);
		}
	}
}
