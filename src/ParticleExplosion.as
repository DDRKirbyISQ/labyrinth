package {
	import net.flashpunk.FP
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Image;
	import flash.display.BitmapData;
	
	public class ParticleExplosion extends Entity {
		[Embed(source='../img/particlemedium.png')]
		private const particleFile:Class;
		[Embed(source='../img/particlelarge.png')]
		private const particleFile2:Class;
		
		private var emitter:Emitter;
		private var emitter2:Emitter;
		private var emitter3:Emitter;
		private var emitter4:Emitter;
		
		public function init(x:Number, y:Number):void {
			this.x = x;
			this.y = y;
			emitter = new Emitter(particleFile, 3, 3);
			emitter.newType("regular", [0]);
			emitter.setAlpha("regular", 1, 0);
			emitter.setMotion("regular", 0, 0, 0, 360, 200, 45);
			emitter.setColor("regular", 0xFFFFFF, 0xFFFFFF);
			emitter2 = new Emitter(particleFile, 3, 3);
			emitter2.newType("regular", [0]);
			emitter2.setAlpha("regular", 1, 0);
			emitter2.setMotion("regular", 0, 0, 0, 360, 200, 45);
			emitter2.setColor("regular", 0x00FF80, 0x00FF80);
			emitter3 = new Emitter(particleFile, 3, 3);
			emitter3.newType("regular", [0]);
			emitter3.setAlpha("regular", 1, 0);
			emitter3.setMotion("regular", 0, 0, 0, 360, 200, 45);
			emitter3.setColor("regular", 0x80FF80, 0x80FF80);
			emitter4 = new Emitter(particleFile2, 8, 8);
			emitter4.newType("regular", [0]);
			emitter4.setAlpha("regular", 1, 0);
			emitter4.setMotion("regular", 0, 0, 0, 360, 100, 30);
			emitter4.setColor("regular", 0x80FF80, 0x80FF80);
			layer = -5000;
			this.graphic = emitter;
			addGraphic(emitter2);
			addGraphic(emitter3);
			addGraphic(emitter4);
			for (var i:int = 0; i < 15; ++i) {
				emitter.emit("regular", 0, 0);
				emitter2.emit("regular", 0, 0);
				emitter3.emit("regular", 0, 0);
				emitter4.emit("regular", 0, 0);
			}
		}
		
		override public function update():void {
			super.update();
			
			if (emitter.particleCount == 0 && emitter2.particleCount == 0 && emitter3.particleCount == 0 && emitter4.particleCount == 0) {
				FP.world.recycle(this);
			}
		}
	}

}