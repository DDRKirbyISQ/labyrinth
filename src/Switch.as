package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Switch extends Transitionable {
		public static var RedEnabled:Boolean = true;
		
		[Embed(source='../img/switchred.png')]
		private static const kRedImageFile:Class;
		private var _redImage:Image = new Image(kRedImageFile);
		
		[Embed(source='../img/switchblue.png')]
		private static const kBlueImageFile:Class;
		private var _blueImage:Image = new Image(kBlueImageFile);
		
		[Embed(source='../img/switchreddown.png')]
		private static const kRedDownImageFile:Class;
		private var _redDownImage:Image = new Image(kRedDownImageFile);
		
		[Embed(source='../img/switchbluedown.png')]
		private static const kBlueDownImageFile:Class;
		private var _blueDownImage:Image = new Image(kBlueDownImageFile);
		
		[Embed(source='../sfx/switch.mp3')]
		private static const kSfxFile:Class;
		
		private var _pressing:Boolean = false;
		
		public function Switch(x:Number, y:Number) {
			super(x, y, _redImage);
			type = "switch";
			layer = 10;
			setHitbox(kWidth, kHeight);
			
			_blueImage.visible = false;
			addGraphic(_blueImage);
			addGraphic(_blueDownImage);
			addGraphic(_redDownImage);
		}
		
		override public function update():void {
			super.update();
			
			_redImage.visible = RedEnabled && !_pressing;
			_redDownImage.visible = RedEnabled && _pressing;
			_blueImage.visible = !RedEnabled && !_pressing;
			_blueDownImage.visible = !RedEnabled && _pressing;

			if (IsTransitioning()) {
				return;
			}
			
			if (collideWith(Player.Instance, x, y) || collide("movingSpike", x, y) || collide("movingSpikeLeft", x, y)) {
				if (!_pressing) {
					RedEnabled = !RedEnabled;
					_pressing = true;
					
					new Sfx(kSfxFile).play();
				}
			} else {
				_pressing = false;
			}
		}
		
		override public function Transition(x:Number, y:Number):void 
		{
			super.Transition(x, y);
			RedEnabled = true;
		}
	}

}
