package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Dialogue extends Entity {
		private static const kYOffset:int = -35;
		
		[Embed(source='../img/dialogue.png')]
		private static const kImageFile:Class;
		private var _image:Image = new Image(kImageFile);
		
		[Embed(source='../sfx/text.mp3')]
		private static const kSfxFile:Class;
		
		private var _source:Entity;
		private var _text:Text;
		private var _targetString:String;
		private var _progress:int = 0;
		
		public function Dialogue(textString:String, source:Entity) {
			_source = source;
			_targetString = textString;
			
			_image.centerOrigin();
			_image.alpha = 0;
			
			super(source.x, source.y, _image);

			_text = new Text(textString);
			_text.size = 8;
			_text.originX = _text.textWidth / 2;
			_text.originY = _text.textHeight / 2;
			_text.color = 0x000000;
			addGraphic(_text);
			
			layer = -1100;
		}
		
		override public function update():void 
		{
			super.update();

			// Update position.
			x = _source.x;
			y = _source.y + kYOffset;
			
			// Update text progress.
			_progress++;
			var length:int = _progress / 5;
			_text.text = _targetString.substr(0, length);
			
			// Play sound.
			if (_progress % 5 == 0 && length < _targetString.length && _targetString.charAt(length) != " ") {
				new Sfx(kSfxFile).play();
			}

			// If we are done, start fading out.
			if (_progress > _targetString.length * 7 + 60) {
				// Fade out...
				_image.alpha -= 0.05;
				if (_image.alpha <= 0) {
					GameWorld.Instance.remove(this);
				}
				return;
			}
			
			_image.alpha += 0.05;
		}
	}
}
