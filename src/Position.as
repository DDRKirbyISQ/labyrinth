package {
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Position {
		public function Position(x:Number, y:Number) {
			X = x;
			Y = y;
		}
		
		public var X:Number;
		public var Y:Number;
	}
}
