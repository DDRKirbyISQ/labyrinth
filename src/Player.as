package {
	import flash.media.Sound;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Particle;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Screen;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Player extends Entity {
		public static var Instance:Player;
		
		private static const kMoveSpeed:Number = 1.0;
		private static const kFallAcceleration:Number = 0.4;
		private static const kFallAccelerationJumping:Number = 0.2;
		private static const kJumpVel:Number = 3;
		private static const kMaxFallSpeed:Number = 4;
		private static const kSpawnPointCount:int = 30;
		
		private var _yVel:Number = 0.0;
		private var _light:Light;
		
		private var _timer:int;
		public var _freezeTimer:int = 0;
		
		private var _spawnPoints:Vector.<Position> = new Vector.<Position>();
		
		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		public var SpritemapObject:Spritemap = new Spritemap(kSpritemapFile, 10, 10);
		
		[Embed(source='../img/playerlight.png')]
		private static const kLightImageFile:Class;
		private var _lightImage:Image = new Image(kLightImageFile);

		[Embed(source='../sfx/land.mp3')]
		private static const kLandSfxFile:Class;
		
		[Embed(source='../sfx/jump.mp3')]
		private static const kJumpSfxFile:Class;
		
		[Embed(source='../sfx/death.mp3')]
		private static const kDeathSfxFile:Class;
		
		[Embed(source='../sfx/step.mp3')]
		private static const kStepSfxFile:Class;
		
		public function Player(x:Number, y:Number) {
			Instance = this;
			
			super(x, y, SpritemapObject);

			setHitbox(6, 8);
			SpritemapObject.originX = 5;
			SpritemapObject.originY = 9;
			originX = halfWidth;
			originY = height;
			type = "player";
			name = "player";
			layer = 0;
			
			AddAnimations();
			SpritemapObject.play("stand");
			
			for (var i:int = 0; i < kSpawnPointCount; ++i) {
				_spawnPoints.push(new Position(x, y));
			}
			
			// Add light.
			_lightImage.originX = _lightImage.width / 2;
			_lightImage.originY = _lightImage.height / 2;
			_light = new Light(x, y, _lightImage, 0.8, 1.0);
			GameWorld.Instance.LightingObject.add(_light);
		}
		
		override public function update():void {
			super.update();
			
			_timer++;
			
			if (_freezeTimer > 0) {
				_freezeTimer--;
				return;
			}
			
			// If we're somehow already colliding (block spawned on top of us) then eject ourselves out.
			while (collide("block", x, y)) {
				y--;
			}
			
			// Handle wrapping
			if (x < 0) {
				x = FP.width - 1;
			}
			if (x > FP.width - 1) {
				x = 0;
			}
			if (y < 0) {
				y = FP.height - 1;
			}
			if (y > FP.height - 1) {
				y = 0;
			}
			
			HandleMovement();
			HandleSpawnPoints();
			
			// Update light position.
			_light.x = x;
			_light.y = y;
		}
		
		public function Freeze():void {
			_freezeTimer = Transitionable.kDuration;
		}
		
		private function HandleSpawnPoints():void {
			// Only insert spawn points if we're grounded
			if (!IsSpawnGrounded()) {
				return;
			}
			
			if (_spawnPoints[_spawnPoints.length - 1].X != x || _spawnPoints[_spawnPoints.length - 1].Y != y) {
				_spawnPoints.shift();
				_spawnPoints.push(new Position(x, y));
			}
		}
		
		public function ResetSpawn():void {
			for (var i:int = 0; i < kSpawnPointCount; ++i) {
				_spawnPoints[i] = new Position(x, y);
			}
		}
		
		public function Die():void {
			var particleExplosion:ParticleExplosion = GameWorld.Instance.create(ParticleExplosion) as ParticleExplosion;
			particleExplosion.init(x, y);

			x = _spawnPoints[0].X;
			y = _spawnPoints[0].Y;
			
			ResetSpawn();
			
			new Sfx(kDeathSfxFile).play();
			
			var flash:ScreenFlash = GameWorld.Instance.create(ScreenFlash) as ScreenFlash;
			flash.reset(0x80FF80, 0.15, 0.01);
		}
		
		private function AddAnimations():void {
			SpritemapObject.add("stand", [2,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,2,0,0,1,0,0], 0.2);
			SpritemapObject.add("walk", [4,5,6,7], 0.25);
			SpritemapObject.add("jump", [3], 0.2, false);
			SpritemapObject.add("land", [2], 0.1, false);
		}
		
		private function HandleMovement():void {
			var isGrounded:Boolean = IsGrounded();
			
			// Jumping
			if (Input.pressed("jump") && isGrounded) {
				_yVel = -kJumpVel;
				new Sfx(kJumpSfxFile).play();
				SpritemapObject.play("jump");
			}
			
			// Gravity.
			if (Input.check("jump")) {
				_yVel += kFallAccelerationJumping;
			} else {
				_yVel += kFallAcceleration;
			}
			if (_yVel > kMaxFallSpeed)
				_yVel = kMaxFallSpeed;
				
			for (var i:int = 0; i < Fan.kRange; ++i) {
				if (collide("fan", x, y + i * Transitionable.kHeight)) {
					_yVel = Math.max( -1, _yVel - kFallAcceleration - 0.3);
					break;
				}
				if (collide("block", x, y + i * Transitionable.kHeight)) {
					break;
				}
			}
			
			if ((SpritemapObject.currentAnim == "jump" || SpritemapObject.currentAnim == "match") && _yVel > 2) {
				SpritemapObject.play("land");
			}
			
			// Try moving in y.
			var oldY:Number = y;
			if (!tryMoveY(_yVel)) {
				if (_yVel > 2) {
					new Sfx(kLandSfxFile).play();
				}
				
				// We collided, so reset our yVel.
				_yVel = 0;
			}
			
			var moveSpeed:Number = kMoveSpeed;
			
			if (Main.DEBUG && Input.check(Key.SHIFT)) {
				moveSpeed *= 3;
			}
			
			if (Input.check(Key.LEFT) && !Input.check(Key.RIGHT)) {
				SpritemapObject.flipped = true;
				SpritemapObject.originX = 4;
				tryMoveX(-moveSpeed);
				if (isGrounded && _yVel == 0) {
					SpritemapObject.play("walk");
					if (_timer % 10 == 0) {
						new Sfx(kStepSfxFile).play(0.6);
					}
				}
				
			} else if (Input.check(Key.RIGHT) && !Input.check(Key.LEFT)) {
				SpritemapObject.flipped = false;
				SpritemapObject.originX = 5;
				tryMoveX(moveSpeed);
				if (isGrounded && _yVel == 0) {
					SpritemapObject.play("walk");
					if (_timer % 10 == 0) {
						new Sfx(kStepSfxFile).play(0.6);
					}
				}
			} else {
				// We're standing still or falling.
				if (isGrounded && _yVel == 0) {
					SpritemapObject.play("stand");
				}
			}
		}
		
		// Returns true if you were able to move, false if you collided.
		public function tryMoveX(amount:Number):Boolean {
			var increment:Number = amount > 0 ? 0.1 : -0.1;
			var progress:Number = 0;
			
			while (Math.abs(progress) < Math.abs(amount)) {
				if (Math.abs(progress) < Math.abs(amount) - Math.abs(increment)) {
					// Move by a bit.
					x += increment;
					progress += increment;
					// If we are colliding, move back and break.
					if (collide("block", x, y) != null) {
						x -= increment;
						return false;
					}
				} else {
					// Move by a bit.
					increment = amount - progress;
					x += amount - progress;
					progress += amount - progress;
					// If we are colliding, move back and break.
					if (collide("block", x, y) != null) {
						x -= increment;
						return false;
					}
				}
			}
			return true;
		}
		
		// Returns true if you were able to move, false if you collided.
		public function tryMoveY(amount:Number):Boolean {
			var increment:Number = amount > 0 ? 0.01 : -0.01;
			var progress:Number = 0;
			
			while (Math.abs(progress) < Math.abs(amount)) {
				if (Math.abs(progress) < Math.abs(amount) - Math.abs(increment)) {
					// Move by a bit.
					y += increment;
					progress += increment;
					// If we are colliding, move back and break.
					if (collide("block", x, y) != null) {
						y -= increment;
						return false;
					}
				} else {
					// Move by a bit.
					increment = amount - progress;
					y += amount - progress;
					progress += amount - progress;
					// If we are colliding, move back and break.
					if (collide("block", x, y) != null) {
						y -= increment;
						return false;
					}
				}
			}
			return true;
		}
		
		public function IsGrounded():Boolean {
			var increment:Number = 3.0;
			
			y += increment;
			
			// If we are colliding, it means we're grounded.
			if (collide("block", x, y) != null) {
				y -= increment;
				return true;
			}
			
			y -= increment;
			
			return false;
		}
		
		public function IsSpawnGrounded():Boolean {
			var increment:Number = 3.0;
			
			y += increment;
			
			// If we are colliding, it means we're grounded.
			var e:Entity = collide("block", x, y);
			if (e != null) {
				y -= increment;
				return e is Block;
			}
			
			y -= increment;
			
			return false;
		}
		
		public function IsCeiled():Boolean {
			var increment:Number = 3.0;
			
			y -= increment;
			
			// If we are colliding, it means we're grounded.
			if (collide("block", x, y) != null) {
				y += increment;
				return true;
			}
			
			y += increment;
			
			return false;
		}
	}
}
