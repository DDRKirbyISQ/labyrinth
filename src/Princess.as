package {
	import flash.media.Sound;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Screen;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Princess extends Entity {
		public static var Instance:Princess;
		
		[Embed(source='../img/princess.png')]
		private static const kSpritemapFile:Class;
		private var _spritemap:Spritemap = new Spritemap(kSpritemapFile, 10, 10);
		
		private static const kFollowDistance:int = 15;
		
		private var _xPositions:Vector.<Number> = new Vector.<Number>;
		private var _yPositions:Vector.<Number> = new Vector.<Number>;
		private var _flipped:Vector.<Boolean> = new Vector.<Boolean>;
		
		public function Princess(x:Number, y:Number) {
			Instance = this;
			
			super(x, y, _spritemap);

			setHitbox(2, 8);
			_spritemap.originX = 5;
			_spritemap.originY = 9;
			originX = 1;
			originY = 10;
			type = "princess";
			name = "princess";
			layer = 10;
			
			addAnimations();
			_spritemap.play("stand");
			
			for (var i:int = 0; i < kFollowDistance; ++i) {
				_xPositions.push(x);
				_yPositions.push(y);
				_flipped.push(false);
			}
		}
		
		override public function update():void {
			super.update();
			
			HandleMovement();
		}
		
		private function addAnimations():void {
			_spritemap.add("stand", [0], 0.15);
			_spritemap.add("walk", [0], 0.1);
			_spritemap.add("jump", [0], 0.2, false);
			_spritemap.add("land", [0], 0.1, false);
		}
		
		private function HandleMovement():void {
			var lastX:Number = _xPositions[_xPositions.length - 1];
			var lastY:Number = _yPositions[_yPositions.length - 1];
			if (IsGrounded() && lastX == Player.Instance.x && lastY == Player.Instance.y) {
				// Don't do anything.
				return;
			}

			x = _xPositions.shift();
			y = _yPositions.shift();
			_spritemap.flipped = _flipped.shift();
			
			_xPositions.push(Player.Instance.x);
			_yPositions.push(Player.Instance.y);
			_flipped.push(Player.Instance.SpritemapObject.flipped);
			
		}
		
		public function IsGrounded():Boolean {
			var increment:Number = 3.0;
			
			y += increment;
			
			// If we are colliding, it means we're grounded.
			if (collide("block", x, y) != null) {
				y -= increment;
				return true;
			}
			
			y -= increment;
			
			return false;
		}
	}
}
