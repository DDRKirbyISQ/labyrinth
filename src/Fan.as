package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Fan extends Transitionable {
		[Embed(source='../img/particletiny.png')]
		private const particleFile:Class;
		
		private var _emitter:Emitter;
		private var _timer:int = 0;
		
		public static const kRange:int = 8;
		
		[Embed(source='../img/fan.png')]
		private static const kSpritemapFile:Class;
		private var _spritemap:Spritemap = new Spritemap(kSpritemapFile, 10, 10);
		
		public function Fan(x:Number, y:Number) {
			super(x, y, _spritemap);
			type = "fan";
			layer = 10;
			setHitbox(kWidth, kHeight);
			_spritemap.add("fan", [0, 1, 2, 3], .25);
			_spritemap.play("fan");

			_emitter = new Emitter(particleFile, 1, 1);
			_emitter.newType("regular", [0]);
			_emitter.setAlpha("regular", 1, 0);
			_emitter.setMotion("regular", 90, 15, 20, 0, 0, 0);
			_emitter.setColor("regular", 0xFFFFFF, 0xFFFFFF);
			addGraphic(_emitter);
		}
		
		override public function update():void {
			super.update();
			
			_timer++;
			if (_timer % 10 == 0) {
				_emitter.emit("regular", FP.rand(10), 6);
			}
		}
	}
}
