package {
	import net.flashpunk.FP;
	import net.flashpunk.Entity;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Levels {	
		[Embed(source='../sfx/transition.mp3')]
		private static const kTransitionSfxFile:Class;
		
		public static function LevelChecks():void {
			for (var level:int = 0; level < kLevelStrings.length; ++level) {
				var blocks:int = 0;
				var spikes:int = 0;
				var keys:int = 0;
				var reds:int = 0;
				var blues:int = 0;
				var switches:int = 0;
				var fans:int = 0;
				
				var levelRows:Array = kLevelStrings[level].split('\n');
				
				for (var row:int = 0; row < levelRows.length; ++row) {
					var rowString:String = levelRows[row];
					
					for (var col:int = 0; col < rowString.length; ++col) {
						var char:String = rowString.charAt(col);
						switch (char) {
							case " ": 
								// Empty
								break;
							case "0":
								blocks++;
								break;
							case "X": 
								spikes++;
								break;
							case "K": 
								keys++;
								break;
							case "R": 
								reds++;
								break;
							case "B": 
								blues++;
								break;
							case "S": 
								switches++;
								break;
							case "F": 
								fans++;
								break;
							case ">":
								break;
							case "<":
								break;
							default: 
								break;
						}
					}
				}
				trace("level " + level + ": " + blocks + " " + spikes + " " + keys + " " + reds + " " + blues + " " + switches + " " + fans);
			}
		}
		
		public static function LoadLevel(level:int):void {
			var levelRows:Array = kLevelStrings[level].split('\n');
			
			for (var row:int = 0; row < levelRows.length; ++row) {
				var rowString:String = levelRows[row];
				
				for (var col:int = 0; col < rowString.length; ++col) {
					var char:String = rowString.charAt(col);
					
					var x:int = col * Transitionable.kWidth;
					var y:int = row * Transitionable.kHeight;
					
					switch (char) {
						case " ": 
							// Empty
							break;
						case "0": 
							GameWorld.Instance.add(new Block(x, y));
							break;
						case "X": 
							GameWorld.Instance.add(new Spike(x, y));
							break;
						case "K": 
							GameWorld.Instance.add(new LevelKey(x, y));
							break;
						case "R":
							GameWorld.Instance.add(new RedBlock(x, y));
							break;
						case "B": 
							GameWorld.Instance.add(new BlueBlock(x, y));
							break;
						case "S": 
							GameWorld.Instance.add(new Switch(x, y));
							break;
						case "F": 
							GameWorld.Instance.add(new Fan(x, y));
							break;
						case ">": 
							GameWorld.Instance.add(new MovingSpike(x, y));
							break;
						case "<": 
							GameWorld.Instance.add(new MovingSpikeLeft(x, y));
							break;
						case "G": 
							GameWorld.Instance.add(new GreenBlock(x, y));
							break;
						case "Y": 
							GameWorld.Instance.add(new YellowBlock(x, y));
							break;
						case "2": 
							GameWorld.Instance.add(new SwitchGreen(x, y));
							break;
						default: 
							break;
					}
				}
			}
		}
		
		public static function LevelTransition(level:int):void {
			Player.Instance.Freeze();
			
			// Delete all keys.
			var keys:Vector.<LevelKey> = new Vector.<LevelKey>;
			GameWorld.Instance.getClass(LevelKey, keys);
			for each (var key:LevelKey in keys) {
				GameWorld.Instance.remove(key);
			}
			
			var oldBlockPositions:Vector.<Position> = new Vector.<Position>;
			var newBlockPositions:Vector.<Position> = new Vector.<Position>;
			var oldSpikePositions:Vector.<Position> = new Vector.<Position>;
			var newSpikePositions:Vector.<Position> = new Vector.<Position>;
			var oldRedPositions:Vector.<Position> = new Vector.<Position>;
			var newRedPositions:Vector.<Position> = new Vector.<Position>;
			var oldBluePositions:Vector.<Position> = new Vector.<Position>;
			var newBluePositions:Vector.<Position> = new Vector.<Position>;
			var oldSwitchPositions:Vector.<Position> = new Vector.<Position>;
			var newSwitchPositions:Vector.<Position> = new Vector.<Position>;
			var oldFanPositions:Vector.<Position> = new Vector.<Position>;
			var newFanPositions:Vector.<Position> = new Vector.<Position>;
			var oldMovingSpikePositions:Vector.<Position> = new Vector.<Position>;
			var newMovingSpikePositions:Vector.<Position> = new Vector.<Position>;
			var oldMovingSpikeLeftPositions:Vector.<Position> = new Vector.<Position>;
			var newMovingSpikeLeftPositions:Vector.<Position> = new Vector.<Position>;
			var oldGreenPositions:Vector.<Position> = new Vector.<Position>;
			var newGreenPositions:Vector.<Position> = new Vector.<Position>;
			var oldYellowPositions:Vector.<Position> = new Vector.<Position>;
			var newYellowPositions:Vector.<Position> = new Vector.<Position>;
			var oldSwitchGreenPositions:Vector.<Position> = new Vector.<Position>;
			var newSwitchGreenPositions:Vector.<Position> = new Vector.<Position>;
			
			// Now transition everything.
           	var levelRows:Array = kLevelStrings[level].split('\n');
			var oldLevelRows:Array = kLevelStrings[level - 1].split('\n');
			for (var row:int = 0; row < levelRows.length; ++row) {
				var rowString:String = levelRows[row];
				var oldRowString:String = oldLevelRows[row];
				
				for (var col:int = 0; col < rowString.length; ++col) {
					var char:String = rowString.charAt(col);
					var oldChar:String = oldRowString.charAt(col);
					
					if (char == oldChar) {
						continue;
					}
					
					var x:int = col * Transitionable.kWidth;
					var y:int = row * Transitionable.kHeight;
					
					switch (char) {
						case "0":
							newBlockPositions.push(new Position(x, y));
							break;
						case "X": 
							newSpikePositions.push(new Position(x, y));
							break;
						case "R": 
							newRedPositions.push(new Position(x, y));
							break;
						case "B": 
							newBluePositions.push(new Position(x, y));
							break;
						case "S": 
							newSwitchPositions.push(new Position(x, y));
							break;
						case "F": 
							newFanPositions.push(new Position(x, y));
							break;
						case ">": 
							newMovingSpikePositions.push(new Position(x, y));
							break;
						case "<": 
							newMovingSpikeLeftPositions.push(new Position(x, y));
							break;
						case "G": 
							newGreenPositions.push(new Position(x, y));
							break;
						case "Y": 
							newYellowPositions.push(new Position(x, y));
							break;
						case "2": 
							newSwitchGreenPositions.push(new Position(x, y));
							break;
						case "K":
							// Special: Just add keys.
							GameWorld.Instance.add(new LevelKey(x, y));
							break;
						default: 
							break;
					}
					switch (oldChar) {
						case "0":
							oldBlockPositions.push(new Position(x, y));
							break;
						case "X": 
							oldSpikePositions.push(new Position(x, y));
							break;
						case "R": 
							oldRedPositions.push(new Position(x, y));
							break;
						case "B": 
							oldBluePositions.push(new Position(x, y));
							break;
						case "S": 
							oldSwitchPositions.push(new Position(x, y));
							break;
						case "F": 
							oldFanPositions.push(new Position(x, y));
							break;
						case ">": 
							oldMovingSpikePositions.push(new Position(x, y));
							break;
						case "<": 
							oldMovingSpikeLeftPositions.push(new Position(x, y));
							break;
						case "G": 
							oldGreenPositions.push(new Position(x, y));
							break;
						case "Y": 
							oldYellowPositions.push(new Position(x, y));
							break;
						case "2": 
							oldSwitchGreenPositions.push(new Position(x, y));
							break;
						default: 
							break;
					}
				}
			}

			// Now move all old position objects to new position objects.
			for each (var position:Position in newBlockPositions) {
				if (oldBlockPositions.length == 0) {
					// No more old blocks, so just generate a new one.
					var rand:Position = RandOutside();
					var block:Block = new Block(rand.X, rand.Y);
					GameWorld.Instance.add(block);
					block.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldBlockPositions.splice(FP.rand(oldBlockPositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newSpikePositions) {
				if (oldSpikePositions.length == 0) {
					// No more old blocks, so just generate a new one.
					var rand:Position = RandOutside();
					var spike:Spike = new Spike(rand.X, rand.Y);
					GameWorld.Instance.add(spike);
					spike.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldSpikePositions.splice(FP.rand(oldSpikePositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("spike", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newRedPositions) {
				if (oldRedPositions.length == 0) {
					// No more old reds, so just generate a new one.
					var rand:Position = RandOutside();
					var red:RedBlock = new RedBlock(rand.X, rand.Y);
					GameWorld.Instance.add(red);
					red.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldRedPositions.splice(FP.rand(oldRedPositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newBluePositions) {
				if (oldBluePositions.length == 0) {
					// No more old blues, so just generate a new one.
					var rand:Position = RandOutside();
					var blue:BlueBlock = new BlueBlock(rand.X, rand.Y);
					GameWorld.Instance.add(blue);
					blue.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldBluePositions.splice(FP.rand(oldBluePositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newSwitchPositions) {
				if (oldSwitchPositions.length == 0) {
					// No more old switchs, so just generate a new one.
					var rand:Position = RandOutside();
					var theSwitch:Switch = new Switch(rand.X, rand.Y);
					GameWorld.Instance.add(theSwitch);
					theSwitch.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldSwitchPositions.splice(FP.rand(oldSwitchPositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("switch", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newFanPositions) {
				if (oldFanPositions.length == 0) {
					// No more old fans, so just generate a new one.
					var rand:Position = RandOutside();
					var fan:Fan = new Fan(rand.X, rand.Y);
					GameWorld.Instance.add(fan);
					fan.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldFanPositions.splice(FP.rand(oldFanPositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("fan", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newMovingSpikePositions) {
				if (oldMovingSpikePositions.length == 0) {
					// No more old movingSpikes, so just generate a new one.
					var rand:Position = RandOutside();
					var movingSpike:MovingSpike = new MovingSpike(rand.X, rand.Y);
					GameWorld.Instance.add(movingSpike);
					movingSpike.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldMovingSpikePositions.splice(FP.rand(oldMovingSpikePositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("movingSpike", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newMovingSpikeLeftPositions) {
				if (oldMovingSpikeLeftPositions.length == 0) {
					// No more old movingSpikeLefts, so just generate a new one.
					var rand:Position = RandOutside();
					var movingSpikeLeft:MovingSpikeLeft = new MovingSpikeLeft(rand.X, rand.Y);
					GameWorld.Instance.add(movingSpikeLeft);
					movingSpikeLeft.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldMovingSpikeLeftPositions.splice(FP.rand(oldMovingSpikeLeftPositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("movingSpikeLeft", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newSwitchGreenPositions) {
				if (oldSwitchGreenPositions.length == 0) {
					// No more old switchGreens, so just generate a new one.
					var rand:Position = RandOutside();
					var switchGreen:SwitchGreen = new SwitchGreen(rand.X, rand.Y);
					GameWorld.Instance.add(switchGreen);
					switchGreen.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldSwitchGreenPositions.splice(FP.rand(oldSwitchGreenPositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("switchGreen", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newYellowPositions) {
				if (oldYellowPositions.length == 0) {
					// No more old yellowBlocks, so just generate a new one.
					var rand:Position = RandOutside();
					var yellowBlock:YellowBlock = new YellowBlock(rand.X, rand.Y);
					GameWorld.Instance.add(yellowBlock);
					yellowBlock.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldYellowPositions.splice(FP.rand(oldYellowPositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}
			for each (var position:Position in newGreenPositions) {
				if (oldGreenPositions.length == 0) {
					// No more old greenBlocks, so just generate a new one.
					var rand:Position = RandOutside();
					var greenBlock:GreenBlock = new GreenBlock(rand.X, rand.Y);
					GameWorld.Instance.add(greenBlock);
					greenBlock.Transition(position.X, position.Y);
					continue;
				}
				var oldPosition:Position = oldGreenPositions.splice(FP.rand(oldGreenPositions.length), 1)[0];
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", oldPosition.X, oldPosition.Y) as Transitionable;
				t.Transition(position.X, position.Y);
			}			
			
			
			// Delete all remaining old things.
			for each (var p:Position in oldBlockPositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}
			// Delete all remaining old things.
			for each (var p:Position in oldSpikePositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("spike", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}
			// Delete all remaining old things.
			for each (var p:Position in oldRedPositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}
			// Delete all remaining old things.
			for each (var p:Position in oldBluePositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}
			// Delete all remaining old things.
			for each (var p:Position in oldSwitchPositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("switch", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}
			// Delete all remaining old things.
			for each (var p:Position in oldFanPositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("fan", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}
			// Delete all remaining old things.
			for each (var p:Position in oldMovingSpikePositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("movingSpike", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}
			// Delete all remaining old things.
			for each (var p:Position in oldMovingSpikeLeftPositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("movingSpikeLeft", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}
			// Delete all remaining old things.
			for each (var p:Position in oldSwitchGreenPositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("switchGreen", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}			
			// Delete all remaining old things.
			for each (var p:Position in oldYellowPositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}			
			// Delete all remaining old things.
			for each (var p:Position in oldGreenPositions) {
				var t:Transitionable = GameWorld.Instance.nearestToPoint("block", p.X, p.Y) as Transitionable;
				t.FadeAway();
			}			
			
			// Play sound.
			new Sfx(kTransitionSfxFile).play(0.6);
		}
		
		public static function RandOutside():Position {
			var r:int = FP.rand(4);
			if (r == 0) {
				return new Position(-10, FP.rand(300));
			} else if (r == 1) {
				return new Position(510, FP.rand(300));
			} else if (r == 2) {
				return new Position(FP.rand(500), -10);
			} else {
				return new Position(FP.rand(500), 310);
			}
		}		

		private static const kLevelStrings:Vector.<String> = new <String>[
/*
12345678901234567890123456789012345678901234567890
*/

// Level 0
new String("\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
00000000000000000000000000000000000000000000000000"),

// Level 1
new String("\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0 0      0   000  0   0 000  0 0   0 00000 0   0 0\n\
0 0     0 0  0  0  0 0  0  0 0 00  0   0   0   0 0\n\
0 0     000  000    0   000  0 0 0 0   0   00000 0\n\
0 0    0   0 0  0   0   0  0 0 0  00   0   0   0 0\n\
0 0000 0   0 000    0   0  0 0 0   0   0   0   0 0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
00000000000000000000000000000000000000000000000000"),

// Level 2
new String("\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                    0                           0\n\
0                    00  0 0                     0\n\
0                    0 0 0 0                     0\n\
0                    00   00                     0\n\
0                        00                      0\n\
0                                                0\n\
0 00  00  00  0 0 0     0        0 0 000  0   0  0\n\
0 0 0 0 0 0 0 00    000 00  0 0 0  0 00  0 0   0 0\n\
0 0 0 0 0 00  00  0 0   0 0 0 0 0  0  00 0 0   0 0\n\
0 00  00  0 0 0 0 0 0   00   00  0 0 000  00  0  0\n\
0                           00              0    0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
00000000000000000000000000000000000000000000000000"),

// Level 3
new String("\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
0                                 0              0\n\
0                                 0              0\n\
0                                 0              0\n\
0      000>     000<     000      0              0\n\
00  00000000000000000000000000    0              0\n\
000                         00  000              0\n\
0000                        00    0              0\n\
00000                       0000  0              0\n\
00000000XX000000XX000000    00    0              0\n\
0                         0000  000              0\n\
0                         0000    0              0\n\
0                       00000000  0              0\n\
0                       000000    0       K      0\n\
0               00    00000000  000     00000    0\n\
0               00    00000000         0000000   0\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 4
new String("\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                       S                        0\n\
0            00 BBBBBB 000 RRRRRR 00             0\n\
0         000                        000         0\n\
0                                       00000    0\n\
0       00XXXXXXXXXXXXXXXXXXXXXXXXXXXXX0000000 S 0\n\
0      0000000000000000000000000000000000000000000\n\
0      0000000000000000000000000000000000000000000\n\
0  K  00000000000000000000000000000000000000000000\n\
0 000 00000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 5
new String("\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0    0  00 00  0                   B             0\n\
0                                  B             0\n\
0  0             0                 B        K    0\n\
0    BBBBBBBBBBB                   B      00000  0\n\
0 S 0XXXXXXXXXXX0                  B     0000000 0\n\
0000000000000000000000  00000000000000000000000000\n\
0                                                0\n\
0                     BB                         0\n\
0                    B                           0\n\
0                  BB                            0\n\
0                                                0\n\
0               BB          RRRRRRRRRRRRRRR      0\n\
0                        000<     000>     000 S 0\n\
0            BB         00000000000000000000000000\n\
0                       00000000000000000000000000\n\
0        00000000000000000000000000000000000000000\n\
0        00000000000000000000000000000000000000000\n\
0       000000000000000000000000000000000000000000\n\
0       000000000000000000000000000000000000000000\n\
0      0000000000000000000000000000000000000000000\n\
0      0000000000000000000000000000000000000000000\n\
0     00000000000000000000000000000000000000000000\n\
0     00000000000000000000000000000000000000000000\n\
0 000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 6
new String("\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                         00000  0\n\
0                          00            0000000 0\n\
0                          00          00000000000\n\
0               0000       00        0000000000000\n\
0               0000       00      000000000000000\n\
0               0000       00    00000000000000000\n\
0      FFF  FFF 0000 FFFF  00    00000000000000000\n\
0     XXXXXXXXXX0000XXXXXXX00FFFF00000000000000000\n\
0     00000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0        >                  X               K    0\n\
0                           X             00000  0\n\
0       FFF  FFF         FFFX   0<     0 0000000 0\n\
000000XXXXXXXXXXXX00000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 7
new String("\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                   RRR  BBB  BBB  RRR           0\n\
0                00 FFF  FFF  FFF  FFF 00        0\n\
0      0>     0  00XXXXXXXXXXXXXXXXXXXX00   S    0\n\
0   0000000000000000000000000000000000000000000000\n\
0                        00000000000             0\n\
00000000000000000000     00000000000             0\n\
0                                                0\n\
0           00000000BBBBB00000000000      00000  0\n\
0     K     00000000FFFFF00000000000     0000000 0\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 8
new String("\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                              >                 0\n\
0                                                0\n\
0                                                0\n\
0         00000000                        S      0\n\
0      000        00         RRRBBBRRR000000000000\n\
0   000            00  GGG  0FFFFFFFFF0          0\n\
0                   00XXXXX000000000000          0\n\
000       GGGGGGG                                0\n\
000   2   BBBBBBB                           K    0\n\
0000000000XXXXXXX00                      0000000 0\n\
0                     000000 YYY YYY 0000000000000\n\
0             YY   00                0000000000000\n\
0             YY   00XXXXXXXXXXXXXXXX0000000000000\n\
0             YY 000000000000000000000000000000000\n\
0     K   2   YY 000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 9
new String("\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
00000000000000000000000                          0\n\
0                                                0\n\
0       0RR00BB00RR00000000                      0\n\
0    K  0XXXXXXXXXX0>  S  0                      0\n\
0  000000000000000000000000 RRR BBB RRR  0000000 0\n\
000000000000000000000000000             0000000000\n\
000000000000000000000000000XXXXXXXXXXXXX0000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 10
new String("\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000                 0\n\
00000000000000000000000000000000                 0\n\
YY                             B                YY\n\
YY                K            B                YY\n\
00000000000000000000000000000000YYY           RR00\n\
00000000000000000000000000000000   YY        RR  0\n\
0000000000000            R           YY     RR   0\n\
     YY                  R             YY  RR     \n\
     YY           S      R                        \n\
0000000000000   0000BBBBBBBBBB00000000000000000000\n\
0              00000                             0\n\
0             000000GG                           0\n\
0            0000000   GGG GGG GGG G             0\n\
0           00000000                G            0\n\
0          000000000                 G           0\n\
0  00000  0000000000  0 X 0 X 0 X 0      2       0\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 11
new String("\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   00000000                   000000\n\
00000000000000   00000000  000000000000000  000000\n\
0000000      0   00000000  000000000000000YY000000\n\
   2    XXXX                                      \n\
00000000000000000000000000000000  0000000000000000\n\
                         0              0         \n\
                         0  0000000000  0         \n\
00000000000000000000000  0  0000000000  0  0000000\n\
0           RRR             000000      00 0000000\n\
0000000  0000000000000000000000000 000000  0000000\n\
   0000          00000000000000000 000000 00      \n\
 S 0000          00000000000000000 000000         \n\
00000000000000   00000000000000000 0000000 0000000\n\
00         000   00000000000000000      0  0000000\n\
00         000   00000000000000000      0 00000000\n\
           000   000000000000000000000  0   BBYY  \n\
00         000   000000000000000000000  0000000000\n\
00    K    000   000000000000000000000  0000000000\n\
00  00000  000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000\n\
00000000000000   000000000000000000000  0000000000"),

// Level 12
new String("\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
000000000000000000000000                         0\n\
000000000000000000000000                         0\n\
0          0><     0                             0\n\
0                                                0\n\
0     0000YYYBBBRRR0000000BBBXXX0000XXXGGG00000 00\n\
0     0000XXXFFFFFF0000000XXXYYY0000RRRXXX0     00\n\
0  K  00000000000000000000000FFF0000FFF0000    000\n\
0000000000000000000000000000000000000000000    000\n\
0                                             0000\n\
0                                             0000\n\
0                                           000000\n\
0                                     BB  00000000\n\
0                                  GG     00000000\n\
0                                     YY  00000000\n\
0                                  RR     00000000\n\
00000000000 RRR GGG BBB YYY 00000         00000000\n\
0000>    S0                 00000         00000000\n\
0000<    20                 00000         00000000\n\
00000000000XXXXXXXXXXXXXXXXX00000XXXXXXXXX00000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 13
new String("\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
0            0000000000000000000000000000000000000\n\
0            0000000000000000000000000000000000000\n\
0            0000000000000000000000000000000000000\n\
0000000000   0000000000000000000000000000000000000\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                                                0\n\
0                       K                        0\n\
0                   000000000                    0\n\
0                  00000000000                   0\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),

// Level 14
new String("\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000\n\
0                                                0\n\
0        YYY Y Y  Y  YYY Y Y  Y Y YYY Y Y        0\n\
0         Y  YYY Y Y Y Y YY    Y  Y Y Y Y        0\n\
0         Y  Y Y Y Y Y Y Y Y   Y  YYY YYY        0\n\
0  F            F                F            F  0\n\
0    BBB BBB BB   BBB B    B  B B BBB BBB BBB    0\n\
0    BB  B B BBB  BBB B   B B  B   B  B B B B    0\n\
0    B   BBB B B  B   BBB B B  B  BBB B B  BB    0\n\
0F              F                F        BBB   F0\n\
0                                                0\n\
0 R      R   RRR  R   R RRR  R R   R RRRRR R   R 0\n\
0 R     R R  R  R  R R  R  R R RR  R   R   R   R 0\n\
0 R     RRR  RRR    R   RRR  R R R R   R   RRRRR 0\n\
0FR   FR   R R  R   R   R  R R R  RR   R  FR   RF0\n\
0 RRRR R   R RRR    R   R  R R R   R   R   R   R 0\n\
0                                                0\n\
0                                                0\n\
0F    F     FFF     FF     FF     FFF     F     F0\n\
0                   000000000                    0\n\
0                S 00000000000 2                 0\n\
00000000000000000000000000000000000000000000000000\n\
0 GG  GG  GG  G G G     G        G G GGG  G   G  0\n\
0 G G G G G G GG    GGG GG  G G G  G GG  G G   G 0\n\
0 G G G G GG  GG  G G   G G G G G  G  GG G G   G 0\n\
0 GG  GG  G G G G G G   GG   GG  G G GGG  GG  G  0\n\
0                           GG              G    0\n\
00000000000000000000000000000000000000000000000000\n\
00000000000000000000000000000000000000000000000000"),
		];
	}
}
