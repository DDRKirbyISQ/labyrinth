package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Block extends Transitionable {
		[Embed(source='../img/block.png')]
		private static const kImageFile:Class;
		private var _image:Image = new Image(kImageFile);
		
		public function Block(x:Number, y:Number) {
			super(x, y, _image);
			type = "block";
			layer = -50;
			setHitbox(kWidth, kHeight);
		}
	}
}
